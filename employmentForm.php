<?php include 'insert.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Document</title>
</head>

<body>
    <header class="navParent primaryBgcolor  ">
        <nav id="navBar" class="navbar navbar-expand-lg navbar-light shadow">
            <a class="navbar-brand mb-0" href="index.php"><img src="images/brainsterLogo.png" class="logo " alt="brainster logo"></a>
            <label for="burgerToggler" class="navbar-toggler ml-auto burger">
                <i id="tooglerIcon" class="fa fa-bars"></i>
                <input type="checkbox" id="burgerToggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            </label>
            <div class="collapse navbar-collapse mt-3 " id="navbarNavAltMarkup">
                <div class="navbar-nav  modifiedNavLink">
                    <a class="nav-link " href="https://marketpreneurs.brainster.co/">Академија за маркетинг </a>
                    <a class="nav-link" href="http://codepreneurs.co/">Aкадемија за програмирање</a>
                    <a class="nav-link" href="#">Академија за data science</a>
                    <a class="nav-link " href="https://design.brainster.co/">Академија за дизајн</a>
                    <a class="nav-link" href="employmentForm.php">Вработи наш студент</a>
                </div>
            </div>
        </nav>
    </header>
    <div class="primaryBgcolor pb-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class=" h1 text-center my-5">Вработи студенти</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <form class="needs-validation mb-5" method="POST" action="employmentForm.php" novalidate>
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-3">
                                <label for="name"> Име и презиме</label>
                                <input type="text" class="form-control" id="name" name="studentName" placeholder="Вашето име и презиме" placeholder="Име и презиме" required>
                               
                                <div class="invalid-feedback">
                                    Задолжително поле!
                                </div>
                            </div>

                            <div class="col-12 col-md-6 mt-3">
                                <label for="companyName">Име на компанија</label>
                                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Име на вашата компанија" required>
                             
                                <div class="invalid-feedback">
                                Задолжително поле!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-3">
                                <label for="name">Контакт имејл</label>
                                <input type="email" class="form-control" id="companyEmail" name="companyEmail" placeholder="Контакт имејл на вашата компанија" required>
                            
                                <div class="invalid-feedback">
                                Задолжително поле!
                                </div>
                            </div>


                            <div class="col-12 col-md-6 mt-3">
                                <label for="companyPhone">Контакт телефон</label>
                                <input type="number" class="form-control" id="companyPhone" name="companyPhone" placeholder="Контакт телефон на вашата компанија" required>
                                <div class="invalid-feedback">
                                Задолжително поле!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-3">
                                <label for="studentType">Тип на студент</label>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-12 col-md-6 ">
                                <select class="custom-select" id="studentType" name="academyType" required>
                                    <option value=''>Избери тип на студент</option>
                                    <option value="Marketing">Маркетинг</option>
                                    <option value="Programming">Програмирање</option>
                                    <option value="Data science">Data science</option>
                                    <option value="Design">Дизајн</option>
                                </select>
                                <div class="invalid-feedback">Избери академија!</div>
                            </div>

                            <div class="col-12 col-md-6 mt-4 mt-lg-0 ">
                                <div class="row">
                                    <div class="col-12 col-lg-9">
                                        <button type="submit" class="btn btn-danger btn-block ">Испрати</button>
                                    </div>
                                    <div class="col-12 col-lg aplicationBtn">
                                        <a href="adminPanel.php" class="btn  btn-outline-dark btn-block "> апликанти</a>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>

    </div>
    <footer class="bg-dark text-light  py-3">
        <div class="d-flex align-items center justify-content-center text-center">
            <b>Изработено со <i class="fa fa-heart" style="color:red"></i> од студентите на Brainster</b>
        </div>
    </footer>

    <script>
        // validacija na forma od bootstrap
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <script src="js/java.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</body>

</html>