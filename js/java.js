// nav bar
document.querySelector('#burgerToggler').addEventListener("change", changeNav);

function changeNav() {
    if (document.querySelector("#burgerToggler").checked) {
        document.getElementById("navBar").classList.add("navChangeOnClick");
        document.getElementById("tooglerIcon").classList.remove("fa", "fa-bars");
        document.getElementById("tooglerIcon").classList.add("fa", "fa-times");
    } else {
        document.getElementById("navBar").classList.remove("navChangeOnClick");
        document.getElementById("tooglerIcon").classList.remove("fa", "fa-times");
        document.getElementById("tooglerIcon").classList.add("fa", "fa-bars");

    }
}
// filter cards
document.querySelector('#showMarketing').addEventListener("change", showMarketing);
document.querySelector('#showPrograming').addEventListener("change", showPrograming);
document.querySelector('#showDesign').addEventListener("change", showDesign);
// show all marketing cards
function showMarketing() {
    document.querySelector("#showDesign").checked = false;
    document.querySelector("#showPrograming").checked = false;
    var allCards = document.querySelectorAll('.cards');
    allCards.forEach(function(card) {
        card.style.display = "none";
    });
    if (document.querySelector("#showMarketing").checked) {
        var cardToShow = document.querySelectorAll('.marketing');
        document.getElementById("bgChange1").classList.add("filteredBox");
        document.getElementById("bgChange2").classList.remove("filteredBox");
        document.getElementById("bgChange3").classList.remove("filteredBox");
    } else {
        var cardToShow = document.querySelectorAll(".cards");
        document.getElementById("bgChange1").classList.remove("filteredBox");
    }
    cardToShow.forEach(function(card) {
        card.style.display = "inline-block";
    })
}
// show all programing cards
function showPrograming() {
    document.querySelector("#showDesign").checked = false;
    document.querySelector("#showMarketing").checked = false;
    var allCards = document.querySelectorAll('.cards');
    allCards.forEach(function(card) {
        card.style.display = "none";
    });
    if (document.querySelector("#showPrograming").checked) {
        var cardToShow = document.querySelectorAll('.programing');

        document.getElementById("bgChange2").classList.add("filteredBox");
        document.getElementById("bgChange1").classList.remove("filteredBox");
        document.getElementById("bgChange3").classList.remove("filteredBox");

    } else {
        var cardToShow = document.querySelectorAll(".cards");
        document.getElementById("bgChange2").classList.remove("filteredBox");

    }
    cardToShow.forEach(function(card) {
        card.style.display = "inline-block";
    })
}
// show all design cards
function showDesign() {
    document.querySelector("#showPrograming").checked = false;
    document.querySelector("#showMarketing").checked = false;
    var allCards = document.querySelectorAll('.cards');
    allCards.forEach(function(card) {
        card.style.display = "none";
    });
    if (document.querySelector("#showDesign").checked) {
        var cardToShow = document.querySelectorAll('.design');
        document.getElementById("bgChange3").classList.add("filteredBox");
        document.getElementById("bgChange1").classList.remove("filteredBox");
        document.getElementById("bgChange2").classList.remove("filteredBox");
    } else {
        var cardToShow = document.querySelectorAll(".cards");
        document.getElementById("bgChange3").classList.remove("filteredBox");
    }
    cardToShow.forEach(function(card) {
        card.style.display = "inline-block";
    })
}
///