<!DOCTYPE html>
<html lang="en">
<?php include 'imageArrays.php'; ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Document</title>
</head>

<body>
    <header class="navParent primaryBgcolor shadow">
        <nav id="navBar" class="navbar navbar-expand-lg navbar-light ">
            <a class="navbar-brand mb-0" href="index.php"><img src="images/brainsterLogo.png" class="logo " alt="brainster logo"></a>
            <label for="burgerToggler" class="navbar-toggler ml-auto burger">
                <i id="tooglerIcon" class="fa fa-bars"></i>
                <input type="checkbox" id="burgerToggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            </label>
            <div class="collapse navbar-collapse mt-3 " id="navbarNavAltMarkup">
                <div class="navbar-nav  modifiedNavLink">
                    <a class="nav-link " href="https://marketpreneurs.brainster.co">Академија за маркетинг </a>
                    <a class="nav-link" href="http://codepreneurs.co">Aкадемија за програмирање</a>
                    <a class="nav-link" href="">Академија за data science</a>
                    <a class="nav-link " href="https://design.brainster.co/">Академија за дизајн</a>
                    <a class="nav-link" href="employmentForm.php">Вработи наш студент</a>
                </div>
            </div>
        </nav>
    </header>

    <div class="bgimg ">
        <div class="d-flex align-items-center justify-content-center h-100 text-center text-light  ">
            <p class="display-3 ">Brainster Labs</p>
        </div>
    </div>
    <!-- choose academy type buttons -->
    <div class="academyTypes">

        <label for="showMarketing" class="defaultBox" id="bgChange1">
            <span>Проекти на студенти по академија за маркетинг</span>
            <i class="fa fa-2x fa-check-circle check-color"></i>
            <input type="checkbox" id="showMarketing">
        </label>
        <label for="showPrograming" class="defaultBox" id="bgChange2">
            <span>Проекти на студенти по академија за програмирање</span>
            <i class="fa fa-2x fa-check-circle check-color"></i>
            <input type="checkbox" id="showPrograming">
        </label>
        <label for="showDesign" class="defaultBox" id="bgChange3">
            <span>Проекти на студенти по академија за дизајн</span>
            <i class="fa fa-2x fa-check-circle check-color"></i>
            <input type="checkbox" id="showDesign">
        </label>
    </div>

    <!-- academy cards -->
    <div class="primaryBgcolor">
        <div class="container pb-5 ">
            <div class="row text-md-center text-left p-4">
                <div class="col">
                    <h1 class="">Проекти</h1>
                </div>
            </div>
            <!-- cards... -->
            <div class="row cardParent">
                <?php for ($i = 0; $i < 10; $i++) { ?>
                    <div class="col-12 col-md-6 col-lg-4 pt-4 programing cards">
                        <div class="card">
                            <!-- card img -->
                            <img src='<?php echo $programingImages[$i]; ?>' class="card-img-top cardImg" alt="Card Image">
                            <div class="card-body p-3">
                                <!-- academy of the card -->
                                <span class="primaryBgcolor p-1 mb-2">
                                    Програмирање
                                </span>
                                <h5 class="card-title">Име на проектот стои овде во две линии</h5>
                                <p class="card-text text-justify ">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.</p>
                                <p class="academyYear">Април - Mарт 2019</p>
                                <a href="#" class="cardBtn float-right "target="_blank">Дознај повеќе</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                for ($x = 0; $x < 4; $x++) { ?>
                    <div class="col-12 col-md-6 col-lg-4 pt-4 design cards">
                        <div class="card">
                            <!-- card img -->
                            <img src='<?php echo $designImages[$x]; ?>' class="card-img-top cardImg" alt="Card Image">
                            <div class="card-body p-3">
                                <!-- academy of the card -->
                                <span class="primaryBgcolor p-1 mb-2">
                                    Дизајн
                                </span>
                                <h5 class="card-title">Име на проектот стои овде во две линии</h5>
                                <p class="card-text text-justify ">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.</p>
                                <p class="academyYear">Април - Октомври 2019</p>
                                <a href="#" class="cardBtn float-right "target="_blank">Дознај повеќе</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                for ($y = 0; $y < 6; $y++) { ?>
                    <div class="col-12 col-md-6 col-lg-4 pt-4 marketing cards">
                        <div class="card">
                            <!-- card img -->
                            <img src='<?php echo $marketingImages[$y]; ?>' class="card-img-top cardImg" alt="Card Image">
                            <div class="card-body p-3">
                                <!-- academy of the card -->
                                <span class="primaryBgcolor p-1 mb-2">
                                    Маркетинг
                                </span>

                                <h5 class="card-title">Име на проектот стои овде во две линии</h5>
                                <p class="card-text text-justify ">Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.</p>
                                <p class="academyYear">Април - Септември 2019</p>
                                <a href="#" class="cardBtn float-right "target="_blank">Дознај повеќе</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <footer class="bg-dark text-light py-3">
        <div class="d-flex align-items center justify-content-center text-center">
            <b>Изработено со <i class="fa fa-heart" style="color:red"></i> од студентите на Brainster</b>
        </div>
    </footer>



</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="js/java.js"></script>

</html>